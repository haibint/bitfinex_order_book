# Usage

### This script is tested under Python 3.7.4
```
pip install websocket_client
python get_order_book.py tBTCUSD
```




Bitfinex Order Book Terms:
https://support.bitfinex.com/hc/en-us/articles/115003284889-Order-Book-Terms#targetText=The%20Ask%20side%20of%20the,is%20at%20that%20price%20point.

Understanding the order book:
An initial order book coming from the response:

data = [158664,[[8499,1,0.15],[8497,1,0.25],[8495.7,2,0.31379861],[8495.2,1,1],[8495,2,1.94636962],[8494.9,1,2],[8494.8,1,0.0294045],[8492.3,1,1.5],[8492,1,0.25],[8491.2,2,1.3],[8491.1,2,0.43333333],[8490.8,1,0.0193],[8490.5,1,5],[8490,1,0.005],[8488.5,1,0.0014574],[8488,1,1.176],[8487.5,1,7.38293748],[8487.1,2,2.06192969],[8487,1,0.25],[8486.5,2,1.15],[8486.3,1,0.0294045],[8486.1,1,0.70703],[8486,1,0.23579144],[8485.8,1,3],[8485.1,2,0.50079999],[8499.1,6,-4.66741148],[8499.2,1,-1.4118976],[8499.3,1,-2],[8499.6,1,-2],[8500,1,-0.48285036],[8500.1,1,-1.245],[8500.9,1,-0.3],[8502.7,2,-0.3],[8502.9,2,-0.87971158],[8503.1,1,-0.3],[8503.3,1,-1.7916],[8503.5,1,-0.9576],[8503.6,1,-0.2],[8503.7,1,-0.3],[8503.8,1,-0.005],[8504,1,-0.5],[8505.2,2,-0.99659736],[8505.3,1,-2.0302],[8505.6,1,-0.02353071],[8506,1,-1.177],[8506.2,1,-0.0788],[8506.3,1,-0.20829752],[8506.7,1,-0.60471083],[8506.8,1,-0.1738],[8507,1,-0.8939]]]
data[0] is channel ID; CHANNEL_ID doesn't change in a session
data[1][:24] are all the bids
data[1][25:] are all the asks

About updates:
- Updates comes for individual price
[CHANNEL_ID, [PRICE, COUNT, AMOUNT]]

[158664,[8488,0,1]]
[158664,[8486,0,1]]
[158664,[8485.8,0,1]]
[158664,[8485.1,0,1]]
[158664,[8499.6,0,-1]]
[158664,[8502.7,0,-1]]
[158664,[8506,0,-1]]
[158664,[8499,1,0.145]]
[158664,[8495,3,2.99636962]]
[158664,[8494.8,1,0.02940485]]
[158664,[8493.1,1,1.68]]
[158664,[8491.3,1,1.69639065]]
[158664,[8487.7,1,1.41192969]]
[158664,[8487.6,2,1.74122597]]
[158664,[8487.1,1,0.65]]
[158664,[8486.3,1,0.02940485]]
[158664,[8499.1,8,-5.6343706]]
[158664,[8500.1,1,-1.27]]
[158664,[8502.8,2,-0.3]]
[158664,[8502.9,1,-0.2]]
[158664,[8503.1,2,-0.97971158]]
[158664,[8504,2,-1.669]]
[158664,[8507.2,1,-2.8174]]
[158664,[8507.5,1,-0.0613394]]


Procedure to update my book:
Algorithm to create and keep a book instance updated

subscribe to channel
receive the book snapshot and create your in-memory book structure
when count > 0 then you have to add or update the price level
3.1 if amount > 0 then add/update bids
3.2 if amount < 0 then add/update asks
when count = 0 then you have to delete the price level.
4.1 if amount = 1 then remove from bids
4.2 if amount = -1 then remove from asks

checksum string format:
If you had bids [{ price: 6000, amount: 1 }, { price: 5900, amount: 2 }] and asks: [{ price: 6100, amount: -3 }, { price: 6200, amount: -4 }], your checksum string would be 6000:1:6100:-3:5900:2:6200:-4.

A node.js full example:
http://blog.bitfinex.com/api/bitfinex-api-order-books-checksums/