from websocket import create_connection
import json
import binascii
import sys

# connect to the socket
ws = create_connection("wss://api-pub.bitfinex.com/ws/2")

ticker = sys.argv[1]
# example: tBTCUSD

#config the socket to enable check sum:
ws.send('{"event": "conf", "flags": 131072}')

#subscribe to a order book.
ws.send('{ "event": "subscribe", "channel": "book", "symbol": "'+ ticker +'", "prec": "P0", "freq":"F1"}')

order_book = {}
channel_id : int

def update_local_book(updating_price_point):
    if updating_price_point["count"] > 0:
        if updating_price_point["amount"] > 0:
            order_book["bids"].update({updating_price_point["price"]: updating_price_point})
        if updating_price_point["amount"] < 0:
            order_book["asks"].update({updating_price_point["price"]: updating_price_point})
    else:
        if updating_price_point["amount"] == 1:
            # print("remove the bid for {}".format(updating_price_point["price"]))
            del order_book["bids"][updating_price_point["price"]]
        if updating_price_point["amount"] == -1:
            # print("remove the ask for {}".format(updating_price_point["price"]))
            del order_book["asks"][updating_price_point["price"]]
    # print(order_book)

def check_checksum(true_checksum):
    print("the checksum (signed) is {}".format(true_checksum))
    unsigned_true_checksum = true_checksum % (1<<32)
    print("true checksum (unsigned) is {}".format(unsigned_true_checksum))
    string_order_book = ""
    # you need to get the sorting right!
    for bid_price, ask_price in zip(sorted(order_book["bids"].keys(), reverse=True), sorted(order_book["asks"].keys())):
        string_order_book += "{}:{}:".format(bid_price, order_book["bids"][bid_price]["amount"])
        string_order_book += "{}:{}:".format(ask_price, order_book["asks"][ask_price]["amount"])
    # take out the last extra colin at the end.
    string_order_book = string_order_book[:-1]
    print(string_order_book)
    if unsigned_true_checksum == binascii.crc32(bytes(string_order_book, "utf-8")):
        print("python checksum is {}".format(binascii.crc32(bytes(string_order_book, "utf-8"))))
        print("checksum succeed")
    else:
        print("python checksum is {}".format(binascii.crc32(bytes(string_order_book, "utf-8"))))
        print("checksum failed")
        


while True:
    msg = json.loads(ws.recv())
    if isinstance(msg, dict):
        print(msg)
        if msg["event"] == "subscribed":
            channel_id = msg["chanId"]
            print("The channel ID is {}".format(channel_id))
    elif isinstance(msg,list) and msg[0] == channel_id:
        if msg[1] == "hb":
            print("just a heart beat.")
        elif msg[1] == "cs":
            check_checksum(msg[2])
        elif len(msg[1]) == 50:
            order_book = {"bids":{}, "asks":{}}
            for bid in msg[1][:25]:
                order_book["bids"].update({bid[0]:{"price":bid[0], "count":bid[1], "amount":bid[2]}})
            for ask in msg[1][25:]:
                order_book["asks"].update({ask[0]:{"price":ask[0], "count":ask[1], "amount":ask[2]}})
            # print(order_book)
        else:
            updating_price_point = {"price":msg[1][0],"count":msg[1][1], "amount":msg[1][2]}
            if isinstance(updating_price_point["count"], list):
                print("this symbol's api does not share the same structure as 'tBTCUSD' and 'tESOBTC'. ")
            update_local_book(updating_price_point)
        
      
          



    